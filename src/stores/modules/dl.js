// 导入 pinia
import { defineStore } from 'pinia'
import { ref } from 'vue'

// 定义 store
export const useDlStore = defineStore(
  'rj-dl',
  () => {
    // 定义在 dl 页面是否开启管理者模式
    const isAdminMode = ref(false)

    /**
     * 设置是否开启管理者模式
     */
    const setIsAdminMode = (value) => {
      isAdminMode.value = value
    }

    // 返回 store
    return {
      isAdminMode,
      setIsAdminMode
    }
  },
  {
    // 配置持久化
    persist: true
  }
)
