// 导入 pinia
import { defineStore } from 'pinia'
import { ref } from 'vue'
import { userGetInfoService, userGetProxyService } from '@/api/user'

// 定义 store
export const useUserStore = defineStore(
  'rj-user',
  () => {
    // 定义一个token
    const token = ref('')
    // 定义一个登录方法
    const setToken = (newToken) => {
      token.value = newToken
    }
    // 定义一个退出登录方法
    const removeToken = () => {
      token.value = ''
    }

    // 定义一个用户信息
    const user = ref({})
    // 定义一个设置用户信息方法
    const setUser = (obj) => (user.value = obj)
    // 定义一个设置用户信息方法
    const getUser = async () => {
      // 获取用户信息
      const res = await userGetInfoService() // 请求获取数据
      // 设置用户信息
      user.value = res.data.data
    }

    // 定义代理
    const proxy = ref([])
    // 设置代理的方法
    const setProxy = (obj) => (proxy.value = obj)
    const getProxy = async () => {
      const res = await userGetProxyService() // 请求获取数据
      proxy.value = res.data.data
    }

    // 返回 store
    return {
      token,
      setToken,
      setUser,
      removeToken,
      getUser,
      user,
      proxy,
      setProxy,
      getProxy
    }
  },
  {
    // 配置持久化
    persist: true
  }
)
