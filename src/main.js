import { createApp } from 'vue'
import { createPinia } from 'pinia'

import persist from 'pinia-plugin-persistedstate'

import App from './App.vue'
import router from './router'

// 导入全局样式文件
import '@/assets/main.scss'
import MusicLoading from '@/components/loading/music-loading/loading'
import PictureLoading from '@/components/loading/picture-loading/loading.js'
import threeDPictureLoading from '@/components/loading/3D-picture-loading/loading.js'

const app = createApp(App)

app.use(createPinia().use(persist))
app.use(router)
app.use(MusicLoading)
app.use(PictureLoading)
app.use(threeDPictureLoading)
app.mount('#app')
