import { createRouter, createWebHashHistory } from 'vue-router'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      component: () => import('@/views/login/LoginPage.vue')
    },
    // 页面布局架子
    {
      path: '/',
      component: () => import('@/views/layout/LayoutContainer.vue'),
      redirect: 'introduce/introducePage',
      // 二级路由
      children: [
        {
          path: 'audio/editLabel',
          component: () => import('../views/audio/EditLabelPage.vue')
        },
        {
          path: 'audio/addCover',
          component: () => import('@/views/audio/AddCoverPage.vue')
        },
        {
          path: 'audio/SymbolRules',
          component: () => import('../views/audio/SymbolRulesPage.vue')
        },
        {
          path: 'introduce/introducePage',
          component: () => import('../views/introduce/IntroducePage.vue')
        },
        {
          path: 'text/RjPage',
          component: () => import('../views/text/RjPage.vue')
        },
        {
          path: 'text/GarbledConvert',
          component: () => import('../views/text/GarbledConvertPage.vue')
        },
        {
          path: 'user/userProfile',
          component: () => import('../views/user/UserProfile.vue')
        },
        {
          path: 'taskList/RjTaskList',
          component: () => import('../views/taskList/RjTaskListPage.vue')
        },
        {
          path: 'database/RjDatabase',
          component: () => import('../views/database/RjDatabasePage.vue')
        },
        {
          path: 'dl/dl-rank',
          component: () => import('../views/dl/dl-rank.vue')
        },
        {
          path: 'dl/dl-favorite-work',
          component: () => import('../views/dl/dl-favorite-work.vue')
        },
        {
          path: 'dl/dl-favorite-list',
          component: () => import('../views/dl/dl-favorite-list.vue')
        },
        // excel匹配
        {
          path: 'taskList/excelMatch',
          component: () => import('@/views/taskList/excel-match-page.vue')
        }
      ]
    },
    {
      path: '/test',
      component: () => import('@/views/test/TestPage.vue')
    }
  ]
})

export default router
