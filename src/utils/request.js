import { useUserStore } from '@/stores/modules/user'
import axios from 'axios'
import router from '@/router'

// 设置请求地址
// 网站的后端地址
// const baseURL = 'http://localhost:8080'
// 测试环境的后端地址
const baseURL = 'http://localhost:8088'

// 创建axios实例
const instance = axios.create({
  // 设置请求地址
  baseURL,
  // 设置超时时间
  // timeout: 100000,
  // 设置超时时间为5分钟
  timeout: 300000,
  withCredentials: true
})

// 请求拦截器
instance.interceptors.request.use(
  // config解释：
  // config是axios请求的配置对象，包含请求地址、请求方法、请求头、请求参数等信息
  (config) => {
    const userStore = useUserStore()
    // 如果用户登录了，则在请求头中添加token
    if (userStore.token) {
      // Authorization解释: 用于表示HTTP协议中的认证信息，通常是一个令牌，用于表明客户端的身份。
      config.headers.Authorization = userStore.token
      // console.log(userStore.token)
    }
    return config
  },
  // Promise解释: 是一个异步的对象，用于承载异步操作的最终结果。
  // 这里的函数接收一个参数，即是axios请求的配置对象config，对config做一些处理，比如添加请求头、修改请求地址等。
  // 直接返回config，表示不对请求做任何处理。
  (err) => Promise.reject(err)
)

// 响应拦截器
instance.interceptors.response.use(
  (res) => {
    // 1 表示成功，0 表示失败
    if (res.data.code === 1) {
      return res
    }
    if (res.data.msg === 'NOTLOGIN' && res.data.code === 0) {
      ElMessage({ message: '请先登录', type: 'error' })
      router.push('/login') // 路由跳转方式来跳转到登录页面
    } else {
      ElMessage({
        message: res.data.msg || '服务异常',
        type: 'error'
      })
      return Promise.reject(res.data)
    }
  },
  (err) => {
    ElMessage({
      message: err.response.data.msg || '服务异常',
      type: 'error'
    })
    // 如果服务端返回的状态码是401，则表示用户未登录，则跳转到登录页面
    if (err.response?.status === 401) {
      router.push('/login')
    }

    return Promise.reject(err)
  }
)

export default instance
export { baseURL }
