// 导入request, 用于发送axios请求
import request from '@/utils/request'

// 乱码转换接口
// 添加新rj到数据库
export const textConvertService = (data) =>
  request.put('/text/convertText', data)
