// 导入request, 用于发送axios请求
import request from '@/utils/request'

// 作品标签匹配接口
export const workEditLabelService = (data) =>
  request.post('/audio/editLabel', data)

// 作品添加封面接口
export const workAddCoverService = (data) =>
  request.post('/audio/addCover', data)
