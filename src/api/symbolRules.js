// 导入request, 用于发送axios请求
import request from '@/utils/request'

/**
 * 添加请求--post
 */
export const addSymbolRulesAPI = (data) =>
  request.post('/symbolRules/add', data)

/**
 * 查看数据库内的所有符号规则--get
 */
export const getSymbolRulesAPI = () =>
  request.get('/symbolRules/list')

/**
 * 修改符号规则--put
 */
export const updateSymbolRulesAPI = (data) =>
  request.put('/symbolRules/update', data)

/**
 * 删除符号规则--delete
 */
export const deleteSymbolRulesAPI = (id) =>
  request.delete('/symbolRules/delete/' + id)
