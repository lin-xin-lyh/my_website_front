// 导入request, 用于发送axios请求
import request from '@/utils/request'

// 注册接口
export const userRegisterService = ({
  username,
  password,
  repassword
}) =>
  request.post('/user/register', {
    username,
    password,
    repassword
  })

// 登录接口
export const userLoginService = ({ username, password }) =>
  request.post('/user/login', { username, password })

// 获取用户信息接口
export const userGetInfoService = () =>
  request.get('/user/info')

// 修改用户信息接口
export const userUpdateInfoService = (data) =>
  request.put('/user/updateInfo', data)

// 上传头像接口
export const userUploadAvatarService = (avatar) =>
  request.patch('/user/uploadAvatar', { avatar })

// 修改密码接口
export const userUpdatePasswordService = (data) =>
  request.put('/user/updatePassword', data)

// 保存代理信息
export const userSaveProxyService = (data) =>
  request.put('/user/saveProxy', data)

// 获取代理信息
export const userGetProxyService = () =>
  request.get('/user/getProxy')
