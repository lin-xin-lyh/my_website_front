// 导入request, 用于发送axios请求
import request from '@/utils/request'

/** favorite-rank */
/**
 * 获取指定时间段的排行数据
 */
export const dlGetRankDataByTime = (timeType) => request.get(`/dl/rank/${timeType}`)

/**
 * 管理员账号密码验证
 */
export const dlAdminConfirm = (data) => request.post('/dl/admin', data)

/**
 * 添加指定的rj到数据库
 */
export const dlAddRj = (data) => request.post('/dl', data)

/**
 * 删除指定的rj
 */
export const dlDeleteRj = (rjNumber) => request.delete(`/dl/${rjNumber}`)

/** favorite-list */
/**
 * 获取收藏列表
 */
export const dlGetFavoriteList = () => request.get('/dl/favorite')

/**
 * 添加收藏
 */
export const dlAddFavorite = (url) => request.post('/dl/favorite', url)

/**
 * 删除收藏
 */
export const dlDeleteFavorite = (rgNumber) => request.delete(`/dl/favorite/${rgNumber}`)

/**
 * 修改收藏
 */
export const dlEditFavorite = (data) => request.put('/dl/favorite', data)

/** favorite-work */
/**
 * 获取 收藏社团的作品列表
 */
export const dlGetFavoriteWork = (rgNumber) => request.get(`/dl/favoriteWork/${rgNumber}`)
