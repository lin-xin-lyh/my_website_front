// 导入request, 用于发送axios请求
import request from '@/utils/request'

// 将 rjTask 的全部内容在 oneWebsite 中匹配查询
export const rjTaskMatchOneWebsiteAPI = () => request.get('/rjTask/matchOneWebsite')

// 将 rjTask 的全部内容在 xyzWebsite 中匹配查询
export const rjTaskMatchXyzWebsiteAPI = () => request.get('/rjTask/matchXyzWebsite')

// 将rj添加到任务列表
export const rjTaskAddRjTaskService = (data) => request.put('/rjTask/addRjTask', data)

// 获取rj任务列表
export const rjTaskGetRjListService = (data) =>
  request.get('/rjTask/getRjTaskList', { params: data })

// 删除rj任务列表
export const rjTaskDeleteRjTaskService = (id) => request.delete(`/rjTask/deleteRjTask/${id}`)

// 编辑任务状态
export const rjTaskEditTaskStatusService = (id, status) =>
  request.put(`/rjTask/editTaskStatus/${id}/${status}`)

// 编辑任务状态
export const rjTaskSetTaskStatusByIdsService = (rjNumbers, status) =>
  request.put('/rjTask/setTaskStatus', {
    rjNumbers,
    status
  })

/**
 * 根据rjNumber在xyz网站中匹配
 */
export const rjTaskMatchXyzWebsiteByRjNumberAPI = (rjNumber) =>
  request.get(`/rjTask/matchXyzWebsite/${rjNumber}`)

/**
 * 根据rjNumber在one网站中匹配
 */
export const rjTaskMatchOneWebsiteByRjNumberAPI = (rjNumber) =>
  request.get(`/rjTask/matchOneWebsite/${rjNumber}`)

/**
 * 在 excel 中匹配 rjNumber
 */
export const rjTaskMatchExcelAPI = () => request.get('/rjTask/matchExcel')

/**
 * 添加 excel 文件到服务器
 */
export const rjTaskAddExcelAPI = (data) => request.post('/rjTask/addExcel', data)

/**
 * 获取 excel 文件匹配结果
 */
export const rjTaskGetExcelMatchRjList = () => request.get('/rjTask/getExcelMatchRjList')

/**
 * 删除 excel 匹配成功的 rj
 */
export const rjTaskDeleteExcelMatchRj = (id) => request.delete(`/rjTask/deleteExcelMatchRj/${id}`)
