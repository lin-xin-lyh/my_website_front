// 导入request, 用于发送axios请求
import request from '@/utils/request'

// rj匹配接口
export const rjTxtMatchService = (data) =>
  request.post('/rj/rjTxtMatch', data)

// 添加新rj到数据库
export const rjAddRjListService = (data) =>
  request.put('/rj/addRjList', data)

// 获取rj数据库信息
export const rjGetRjDatabaseService = (data) =>
  request.get('/rj/getRjDatabase', { params: data })

// 爬取rj信息
export const rjCrawlRJInfoService = (data) =>
  request.post('/rj/crawlRJInfo', data)

// 爬取所有rj信息
export const rjCrawlAllRJInfoService = () =>
  request.post('/rj/crawlAllRJInfo')
